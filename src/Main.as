package 
{
	import flash.display.Sprite;
	import states.BlueState;
	import states.IState;
	import states.RedState;
	
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			var state:IState;
			
			state = new BlueState(this);
			state.engage();
			
			// We switch state
			
			state = new RedState(this);
			state.engage();
		}
		
		public function sayBlue():void 
		{
			trace("I am BLUE");
		}
		
		public function sayRed():void
		{
			trace("I am RED");
		}
	}	
}