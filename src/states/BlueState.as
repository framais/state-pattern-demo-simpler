package states 
{
	public class BlueState implements IState
	{
		private var _main:Main;
		
		public function BlueState(main:Main)
		{
			_main = main;
		}
		
		public function engage():void
		{
			_main.sayBlue(); 
		}
	}
}