package states 
{
	public interface IState 
	{
		
		function engage():void;
	}	
}