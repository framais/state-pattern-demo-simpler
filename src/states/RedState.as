package states 
{
	public class RedState implements IState
	{
		private var _main:Main;
		
		public function RedState(main:Main)
		{
			_main = main;
		}
		
		public function engage():void
		{
			_main.sayRed(); 
		}
	}
}